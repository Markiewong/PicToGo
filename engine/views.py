from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from .models import *
from django.utils import timezone
from datetime import date, datetime, timedelta
from os import listdir
from django.conf import settings as pictogoSettings
from PIL import Image
from io import BytesIO
from io import open as iopen

import requests

# Create your views here.
# The views for the main screen


def index(request):
    today = {'targetDate': date.today()}
    rtheme = requests.get(
        'http://localhost:8080/themes/findByTargetDate', params=today).json()[0]
    currentTheme = rtheme.get('themeItem')

    rdate = requests.get('http://localhost:8080/themes').json()['_embedded']
    for theme in rdate['themes']:
        if theme['themeItem'] == currentTheme:
            endDate = theme['endDateOfTheme']

        else:
            endDate = ('There is no end date')

    # themesList = Theme.objects.filter(
    #     startDate__lte=timezone.now(), endDate__gt=timezone.now())
    # if len(themesList) > 0:
    #     theme = themesList[0].name
    #     endDate = themesList[0].endDate
    # else:
    #     theme = "There is no theme"
    #     endDate = "There is no enddate"

    r = requests.get('http://localhost:8080/metas').json()['_embedded']
    imagenames = []
    for meta in r['metas']:
        if meta['theme'] == theme and meta['active'] == True:
            imagenames.append(meta['imageURI'])

    context = {'theme': currentTheme,
               'endDate': endDate, 'imagenames': imagenames}
    return render(request, 'engine/mainScreen.html', context)


def photos(request):
    return HttpResponse("This is the photos page")

# The views for the settings part


def settings(request):
    return render(request, 'engine/settings.html')


def settingsThemes(request):
    # POST (save)
    themename = request.POST.get("themename")
    startdate = request.POST.get("startdate")
    enddate = request.POST.get("enddate")
    theme = {
        'themeItem': themename, 'startDateOfTheme': startdate, 'endDateOfTheme': enddate}
    if themename != None:
        rpost = requests.post('http://localhost:8080/themes', json=theme)
        print(rpost.text)

    # GET
    rget = requests.get('http://localhost:8080/themes').json()['_embedded']
    themes = []
    for theme in rget['themes']:
        themes.append(theme)

    context = {'themes': themes}
    return render(request, 'engine/settingsThemes.html', context)


def settingsThemesDelete(request, themePk=None):
    # DELETE
    rdelete = requests.delete('http://localhost:8080/themes/' + themePk)
    return redirect('settingsThemes')


def settingsResidents(request):
    return render(request, 'engine/settingsResidents.html')

#Settings for the photos. E.G. Validate and delete pictures.

def settingsPhotos(request):
    r = requests.get('http://localhost:8080/metas').json()['_embedded']
    imagenames = []
    for meta in r['metas']:
        if meta['active'] == False:
            imagenames.append(meta['imageURI'])

    context = {'imagenames': imagenames}
    return render(request, 'engine/settingsPhotos.html', context)

def settingsPhotosDelete(request, photoPk=None):
    # DELETE
    rdelete = requests.delete('http://localhost:8080/metas/' + photoPk)
    return redirect('settingsPhotos')

# The views for the profiles


def profilesList(request):
    return render(request, 'engine/profilesList.html')


def profile(request):
    return render(request, 'engine/profile.html')

# The views for the slideshow


def slideshowThemes(request):
    r = requests.get('http://localhost:8080/themes').json()['_embedded']
    themenames = []
    for theme in r['themes']:
        themenames.append(theme['themeItem'])
    context = {'themenames': themenames}
    return render(request, 'engine/slideshowThemes.html', context)


def slideshow(request, theme=None):

    r = requests.get('http://localhost:8080/metas').json()['_embedded']
    filenames = []
    for meta in r['metas']:
        if meta['theme'] == theme and meta['active'] == True:
            filenames.append(meta['imageURI'])
    context = {'filenames': filenames}
    return render(request, 'engine/slideshow.html', context)

# The views for the photo album


def photoAlbum(request):
    return render(request, 'engine/photoAlbum.html')

    # The views for debug/test purposes


def test(request):
    return render(request, 'engine/test.html')
