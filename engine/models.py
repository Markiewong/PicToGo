from django.db import models

# Create your models here.
class Theme(models.Model):
    name=models.CharField(max_length=256)
    startDate=models.DateField()
    endDate=models.DateField()
