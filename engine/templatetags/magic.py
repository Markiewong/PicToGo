from django import template

register = template.Library()


@register.filter(name='get')
def get(d, k):
    return d.get(k, None)


@register.filter(name='getId')
def getId(d):
    return d[d.rfind('/')+1:len(d)]