from django.urls import path, re_path

from . import views


urlpatterns = [
    # /engine/
    re_path(r'^$', views.index, name='index'),
    re_path(r'^photos/$', views.photos, name='photos'),

    re_path(r'^settings/$', views.settings, name='settings'),
    re_path(r'^settings/themes/$', views.settingsThemes, name='settingsThemes'),
    re_path(r'^settingsdeletetheme/(?P<themePk>.+)$', views.settingsThemesDelete, name='deleteUrl'),
    re_path(r'^settings/residents/$', views.settingsResidents,
            name='settingsResidents'),
    re_path(r'^settings/photos/$', views.settingsPhotos, name='settingsPhotos'),

    re_path(r'^slideshowThemes/$', views.slideshowThemes, name='slideshowThemes'),
    re_path(r'^slideshow/(?P<theme>.+)$', views.slideshow, name='slideshow'),

    re_path(r'^profilesList/$', views.profilesList, name='profilesList'),
    re_path(r'^profile/$', views.profile, name='profile'),

    re_path(r'^photoAlbum/$', views.photoAlbum, name='photoAlbum'),

    re_path(r'^test/$', views.test, name='test'),
]
